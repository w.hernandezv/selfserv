"""selfserv URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from . import views

from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name='index'),
    path('neworder/', views.neworder, name='neworder'),
    path('order/<int:id>/', views.order, name='order'),
    path('order/<int:idorder>/add/<int:idproduct>/', views.add, name='add'),
    path('order/<int:idorder>/remove/<int:idproduct>/', views.remove, name='remove'),
    path('order/<int:idorder>/payment/', views.payment, name='payment'),
    path('order/<int:idorder>/clear/', views.clearorder, name='clearorder'),
    path('order/<int:idorder>/paid/', views.paidorder, name='paidorder'),
    path('login/', views.login, name='login'),
    path('login/auth', views.auth, name='auth'),
    path('logout', views.logout, name='logout'),
    path('kitchen/', views.kitchen, name='kitchen'),
    path('order/<int:idorder>/paymethod/', views.paymethod, name='paymethod'),
    path('kitchen/changestate/<int:idorder>/', views.paidtowaiter, name='paidtowaiter'),
    path('waiter/', views.waiter, name='waiter'),
    path('waiter/changestate/<int:idorder>/', views.waitertodelivered, name='waitertodelivered'),
    path('order/<int:idorder>/subtractone/<int:idproduct>/', views.subtractone, name='subtractone')
    

] + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)