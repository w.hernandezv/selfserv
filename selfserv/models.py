from django.db import models

class Category(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False)
    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name_plural = "Categories"

class Product(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False)
    base_price = models.IntegerField()
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    photo = models.ImageField(upload_to='products', blank=True, null=True)
    
    def __str__(self):
        return self.name

CREATED = "Creada"
PAID = "Pagada"
WAITER = "Mesero"
DELIVERED = "Entregada"



STATE_CHOICES = (
    (CREATED, "Creada"),
    (PAID, "Pagada"),
    (WAITER, "Mesero"),
    (DELIVERED, "Entregada")
)


class Order(models.Model):
    state = models.CharField(max_length=100, choices=STATE_CHOICES, default=CREATED)

    def __str__(self):
        return "Order no. " + str(self.id)
    def get_total(self):
        productosorden = OrderProduct.objects.filter(order = self)
        productos = Product.objects.all()
        total = 0
        for i in productosorden:
            elprod = Product.objects.get(id = i.product.id)
            total = total + elprod.base_price*i.quantity
        return total

    def get_cantidad_productos(self):
        productosorden = OrderProduct.objects.filter(order = self)
        productos = Product.objects.all()
        total = 0
        for i in productosorden:
            total = total + 1
        return total

class OrderProduct(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField(default=1)

    def __str__(self):
        return self.product.name + ' (' + str(self.quantity) + ')'
    
    def get_total(self):
        total = 0
        for i in self.product:
            total = total + self.product.base_price * self.quantity
        return total

    def get_tot_prod(self):
        total = 0
        total = self.product.base_price * self.quantity
            
        return total

    class Meta:
        unique_together = ('order', 'product') #Orden solo puede tener una instancia de cada producto

