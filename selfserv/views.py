from django.shortcuts import render
from django.http import HttpResponse
from .models import Category, Product, OrderProduct, Order
from django.shortcuts import redirect
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib.auth import login as django_login
from django.contrib.auth import logout as django_logout
from . import models

def index(request):
    return render(request, 'index.html')

def order(request, id):

    order = Order.objects.get(id = id)
    orderproducts = OrderProduct.objects.filter(order = order)
    #a = OrderProduct.objects.get(order = order).get_total()
    products = Product.objects.all()
    categories = Category.objects.all()
    orden = Order.objects.get(id = 1)
    total = OrderProduct.objects.filter(order = orden)
    tot = orden.get_total()

    if order.state == models.CREATED:
        return render(request, 'order.html', {'products':products, 'categories':categories,'total':tot,'order':order,'orderproducts':orderproducts})
    else:
        return render(request, 'alreadypaid.html')

def neworder(request):
    order = Order()
    order.save()
    return redirect('/order/'+str(order.id))

#add product to order
def add(request, idorder, idproduct):
    order = Order.objects.get(id = idorder)
    product = Product.objects.get(id = idproduct)

    #try adding one if it already exists
    try:
        op = OrderProduct.objects.get(order = order, product = product)
        op.quantity = op.quantity + 1
        op.save()
        return redirect('/order/'+str(order.id))
    #adding a completely new product to the order
    except:
        op = OrderProduct(order = order, product = product)
        op.save()
        return redirect('/order/'+str(order.id))

def remove(request, idorder, idproduct):
    order = Order.objects.get(id = idorder)
    product = Product.objects.get(id = idproduct)
    op = OrderProduct.objects.get(order = order, product = product)
    op.delete()
    return redirect('/order/'+str(order.id))

def payment(request, idorder):
    order = Order.objects.get(id = idorder)
    orderproducts = OrderProduct.objects.filter(order = order)

    if not order.state == models.CREATED:
        return render(request, 'alreadypaid.html')

    if order.get_total() > 0:
        return render(request, 'payment.html', {'order':order, 'orderproducts':orderproducts})
    else:
        return HttpResponse('Debes agregar al menos un producto<br><a href="/order/'+ str(idorder) +'">Volver</a>')

def clearorder(request, idorder):
    order = Order.objects.get(id = idorder)

    orderproducts = OrderProduct.objects.filter(order = order)
    for i in orderproducts:
        i.delete()

    #print(orderproducts)
    return redirect('/order/'+str(idorder))

def paidorder(request, idorder):
    order = Order.objects.get(id = idorder)
    if order.state == models.CREATED:
        order.state = models.PAID
        order.save()
        print("xd")

    return render(request, 'paid.html')

def kitchen(request):
    if request.user.is_authenticated:
        #print(request.user.is_authenticated)
        orderproducts = OrderProduct.objects.all()
        products = Product.objects.all()
        orders = Order.objects.filter(state = models.PAID)

        return render(request, 'kitchen.html', {'orders':orders,'orderproducts':orderproducts,'products':products})
    else:
        return redirect('/login/')

def login(request):
    return render(request, 'login.html')

def auth(request):
    user = authenticate(username=request.POST.get('username',''), password=request.POST.get('pass',''))
    if user is not None:
        django_login(request, user)
        return redirect('/')
    else:
        return redirect('/login/')

def logout(request):
    django_logout(request)
    return redirect('/')

def paymethod(request, idorder):
    order = Order.objects.get(id = idorder)

    if order.state == models.CREATED:
        return render(request, 'paymethod.html')
    else:
        return render(request, 'alreadypaid.html')

def paidtowaiter(request, idorder):
    order = Order.objects.get(id = idorder)

    if order.state == models.PAID:
        order.state = models.WAITER
        order.save()
        return redirect('/kitchen/')

def waiter(request):
    if request.user.is_authenticated:
        orderproducts = OrderProduct.objects.all()
        products = Product.objects.all()
        orders = Order.objects.filter(state = models.WAITER)
        return render(request, 'waiter.html', {'orders':orders,'orderproducts':orderproducts,'products':products})
    else:
        return redirect('/login/')

def waitertodelivered(request, idorder):
    order = Order.objects.get(id = idorder)

    if order.state == models.WAITER:
        order.state = models.DELIVERED
        order.save()
        return redirect('/waiter/')

def subtractone(request, idorder, idproduct):
    order = Order.objects.get(id = idorder)
    product = Product.objects.get(id = idproduct)
    op = OrderProduct.objects.get(order = order, product = product)

    if op.quantity == 1:
        op.delete()
    else:
        op.quantity = op.quantity - 1
        op.save()

    return redirect('/order/'+str(order.id))
